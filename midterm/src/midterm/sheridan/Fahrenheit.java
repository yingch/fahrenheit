package midterm.sheridan;


/**
 * 
 * @author Ying Chen 991549628
 *
 */
public class Fahrenheit {
	public static int fromCelsius(int celsius) {
		// the minimum Celsius is 273.15 degree
		if (celsius < -273) {
			throw new IllegalArgumentException("celisus can not be less than -273 degree");
		}
		return (int)Math.ceil( (celsius * 9 * 1.0) / 5 ) + 32;
	}
}
