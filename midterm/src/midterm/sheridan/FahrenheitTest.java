package midterm.sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Ying Chen 991549628
 *
 */
public class FahrenheitTest {

	@Test
	public void testFromCelsiusRegular() {
		assertTrue("Output degree is not correct", Fahrenheit.fromCelsius(32) == 90  );
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFromCelsiusException() {
		assertFalse("Input Celsius is invalid", Fahrenheit.fromCelsius(-400) == 91 );
	}

	@Test
	public void testFromCelsiusBoundaryIn() {
		assertTrue("Output degrees are not correct", Fahrenheit.fromCelsius(0) == 32  );
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFromCelsiusBoundaryOut() {
		// the minimum input is 273 degree
		assertTrue("Input Celsius is invalid", Fahrenheit.fromCelsius(-274) == 90  );
	}

}
